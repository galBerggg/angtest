export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCGsYWfWT8GTcXI-zd0V9i0K9a4Ftx1E8o",
    authDomain: "aggg-4698e.firebaseapp.com",
    databaseURL: "https://aggg-4698e.firebaseio.com",
    projectId: "aggg-4698e",
    storageBucket: "aggg-4698e.appspot.com",
    messagingSenderId: "422153082663",
    appId: "1:422153082663:web:6940d31d02a4947cb07b02",
    measurementId: "G-4VMVDTNC6Q"
  }
};